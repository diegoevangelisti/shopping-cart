import React from 'react';
import { useSelector } from 'react-redux'
import ViewProducts from './components/ViewProducts.js'
import ShoppingCart from './components/ShoppingCart'
import { Container, Navbar, NavDropdown, Nav } from 'react-bootstrap'
import './style/App.css'

const products = [
  { 'name': 'Sledgehammer', 'price': 125.75 },
  { 'name': 'Axe', 'price': 190.95 },
  { 'name': 'Bandsaw', 'price': 562.13 },
  { 'name': 'Chisel', 'price': 12.9 },
  { 'name': 'Hacksaw', 'price': 18.45 }
]

const App = () => {

  const state = useSelector(state => state)
  const { cart } = state

  const totalPriceCalculation = () => {
    let totalPrice = 0
    if (cart && cart.length > 0) {
      cart.map(cart => {
        totalPrice = totalPrice + cart.price * cart.quantity
      })
      return totalPrice
    }
  }

  return (
    <div>
      <Container fluid>
        <Navbar collapseOnSelect expand="lg" bg="light" variant="light">
          <Navbar.Brand href="#home">React Task</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <NavDropdown title="Cart" className="dropdownMenu" id="collasible-nav-dropdown">
                <ShoppingCart totalPrice={totalPriceCalculation()} />
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <ViewProducts products={products} />
      </Container>
    </div>
  );
}

export default App;
