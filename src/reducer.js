const initialState = {
    cart: []
};

const reducer = (state = initialState, action) => {
    const { type, payload } = action
    let cart = state.cart ? state.cart : []

    switch (type) {
        case 'ADD_TO_CART': {
            var isProductInCart = false
            var newCartProduct = {}
            cart.forEach((cartProduct, index) => {
                if (payload.name === cartProduct.name) {
                    newCartProduct = {
                        key: index,
                        name: payload.name,
                        price: payload.price,
                        quantity: cartProduct.quantity + 1
                    }
                    isProductInCart = true
                }
            })
            if (!isProductInCart) {
                newCartProduct = {
                    key: cart.length,
                    name: payload.name,
                    price: payload.price,
                    quantity: 1
                }
            }
            cart = cart.filter(cartProduct => cartProduct.name !== payload.name)
            cart.push(newCartProduct)

            // Filter to keep consistency in the cart order
            cart.sort(function (a, b) {
                return a.key - b.key
            })
            return Object.assign({}, state, { cart: cart })
        }
        case 'REMOVE_FROM_CART': {
            cart = cart.filter(cartProduct => cartProduct.name !== payload.name)

            // Reorder the product keys to show cart items in correct order
            cart.forEach((cartProduct, index) => {
                cartProduct.key = index
            })
            return Object.assign({}, state, { cart: cart })
        }
        default: return Object.assign({}, state, { cart: cart })
    }
}
export default reducer
