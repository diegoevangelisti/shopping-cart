import React from 'react'
import Button from 'react-bootstrap/Button'
import { useDispatch } from 'react-redux'
import PropTypes from 'prop-types';
import '../style/Product.css'


const Product = props => {
    const { name, price } = props
    const dispatch = useDispatch()

    const increaseQuantity = () => {
        dispatch({ type: 'ADD_TO_CART', payload: { name, price } })
    }
    return (
        <div className="product">
            <p id="name">{name}</p>
            <p id="price">$ {Number(price).toFixed(2)}</p>
            <Button
                className="Button"
                onClick={() => increaseQuantity()}
            >Add
            </Button>
        </div>
    )
}

Product.propTypes = {
    name: PropTypes.string,
    price: PropTypes.number,
};

export default Product;

