import React from 'react'
import { useSelector } from 'react-redux'
import CartProduct from './CartProduct'
import PropTypes from 'prop-types';
import { NavDropdown } from 'react-bootstrap';


const ShoppingCart = props => {
    const { totalPrice } = props
    const cart = useSelector(state => state.cart)

    return (
        <>
            {cart.length > 0 && (cart.map(cartProduct => (
                <NavDropdown.Item active={false}>
                    <CartProduct
                        name={cartProduct.name}
                        price={cartProduct.price}
                        quantity={cartProduct.quantity}
                    />
                </NavDropdown.Item>)))
            }
            {cart.length === 0 && (
                <NavDropdown.Item active={false}>
                    Empty cart
                </NavDropdown.Item>
            )}
            {cart.length > 0 && (
                <NavDropdown.Item active={false}>
                    <li>Total price: $ {totalPrice.toFixed(2)}</li>
                </NavDropdown.Item>
            )}
        </>
    );
}

ShoppingCart.propTypes = {
    cartProducts: PropTypes.array,
    totalPrice: PropTypes.number
};

export default ShoppingCart;
