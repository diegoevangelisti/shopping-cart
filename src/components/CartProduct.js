import React from 'react'
import { useDispatch } from 'react-redux'
import PropTypes from 'prop-types';
import '../style/CartProduct.css'


const CartProduct = props => {
    const { name, price, quantity } = props

    const total = (price * quantity).toFixed(2)
    const dispatch = useDispatch()

    const removeProductFromCart = () => dispatch({ type: 'REMOVE_FROM_CART', payload: { name } })

    return (
        <div>
            <li>
                <span>{name}</span>
                <span>$ {price}</span>
                <span> Quantity: {quantity}</span>
                <span> Total: $ {total}</span>
                <ion-icon
                    id="deleteIcon"
                    name="trash-sharp"
                    onClick={() => removeProductFromCart()}>
                </ion-icon>
            </li>
        </div>
    )
}

CartProduct.propTypes = {
    name: PropTypes.string,
    price: PropTypes.number,
    quantity: PropTypes.number
};

export default CartProduct;

