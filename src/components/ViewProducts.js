import React from 'react'
import Product from './Product'
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap'


const ViewProducts = props => {
    const { products } = props

    return (
        <Row >
            {products.map(product => (
                <Col xs={12} md={6} lg={4}>
                    <Product
                        name={product.name}
                        price={product.price}
                    />
                </Col>
            ))}
        </Row>
    );
}

ViewProducts.propTypes = {
    products: PropTypes.array,
};

export default ViewProducts;
